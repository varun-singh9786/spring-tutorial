/**
 * 
 */
package org.koushik.javabrains.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.koushik.javabrains.model.Circle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author varun
 *
 */
@Component
public class JdbcDaoImpl {
	
	/**
	 * dataSource
	 */
	@Autowired
	private DataSource dataSource;
	
	/**
	 * 
	 * @param circleId
	 * @return
	 */
	public Circle getCircle(int circleId) {
		
		Connection conn = null;
		
		try {
			conn = this.getDataSource().getConnection();
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM circle where id=?");
			ps.setInt(1, circleId);
			
			Circle circle = null;
			
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				circle = new Circle(circleId, rs.getString("name"));
			}
			
			rs.close();
			ps.close();
			return circle;
		} catch (Exception e) {
			System.out.println("Exception raised");
			throw new RuntimeException(e);
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
			
		}
		
		
		
	}

	/**
	 * @return the dataSource
	 */
	public DataSource getDataSource() {
		return dataSource;
	}

	/**
	 * @param dataSource the dataSource to set
	 */
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
}
