/**
 * 
 */
package org.koushik.javabrains;

import org.koushik.javabrains.dao.JdbcDaoImpl;
import org.koushik.javabrains.model.Circle;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author varun
 *
 */
public class JdbcDemo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("spring.xml");
		JdbcDaoImpl daoImpl = ctx.getBean("jdbcDaoImpl", JdbcDaoImpl.class);
		
		Circle circle = daoImpl.getCircle(1);
		System.out.println(circle.getName());

	}

}
