/**
 * 
 */
package org.koushik.javabrains.model;

/**
 * @author varun
 *
 */
public class Circle {
	/**
	 * id
	 */
	private int id;
	
	/**
	 * Name
	 */
	private String name;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param id
	 * @param name
	 */
	public Circle(int id, String name) {
		super();
		setId(id);
		setName(name);
	}
	
	

}
